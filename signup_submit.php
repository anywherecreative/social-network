<?php
	require('connect.php');
	$userName = $mysql->real_escape_string($_POST['user_name']);
	$result = $mysql->query("SELECT *FROM `users` WHERE `USER_NAME` = '$userName'");
	if($result->num_rows > 0) {
		die('User already exists');
	}
	$firstName = $mysql->real_escape_string($_POST['first_name']);
	if(trim(len($firstName)) < 1) {
		die('First Name can\'t be empty');
	}
	$lastName = $mysql->real_escape_string($_POST['last_name']);
	if(trim(strlen($lastName)) < 1) {
		die('First Name can\'t be empty');
	}
	$email = $mysql->real_escape_string($_POST['email']);
	if(stripos($email, '@') === false || stripos($email, '.') === false) {
		die('Not a valid email');
	}
	$result = $mysql->query("SELECT *FROM `users` WHERE `USER_EMAIL` = '$email'");
	if($result->num_rows > 0) {
		die('Email already exists');
	}
		$pass1 = $_POST['password'];
		$pass2 = $_POST['password2'];
	if($pass1 != $pass2) {
		die('passwords don\'t match');
	}
?>