<?php
if(!isset($_GET['kfried'])) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">

<html>
<head>
    <title>No Entry Man</title>
</head>

<body>

<?php
	echo('<pre>');
	echo(system('fortune | cowsay'));
	echo('</pre>');
	exit;
?>
</body>
</html>
<?php
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Comments!</title>
<script src='http://code.jquery.com/jquery-2.1.1.min.js'></script>
<script>
	$(document).ready(function() {
		$('#commentBox BUTTON').click(function () {
			postComment();
		});
		$('#commentBox TEXTAREA').on('keyup blur',function() {
			if($('#commentBox TEXTAREA').val() == '') {
				$('#commentBox BUTTON').hide();
			}
			else {
				$('#commentBox BUTTON').show();
			}
		});
		$('#commentBox TEXTAREA').keypress(function (e) {
			code = (e.keyCode ? e.keyCode : e.which);
			if (code == 13 && $('#ets').is(":checked")) {
				postComment()
			}
		});
		refreshComments();
		$('#commentBox BUTTON').hide();
		setInterval(refreshComments, 5000);
	});
		
	function refreshComments() {
		$.get('get_comments.php', function( data ) {
			$('#comments').html(data);
		});
	}
	
function postComment() {
	if($('#commentBox TEXTAREA').val() == '') {
		alert('You can\'t post an empty comment!');
		return false;
	}
	if($('#commentBox TEXTAREA').val() == 'FLIP') {
           $('BODY .container').toggleClass('flip');
		   return true;
	}
	if($('#commentBox TEXTAREA').val() == 'SPIN') {
           $('BODY .container').toggleClass('rotate');
		   return true;
	}
	if($('#commentBox TEXTAREA').val() == 'LIGHTS OUT!') {
           $('BODY').toggleClass('dark');
		   return true;
	}
	$.ajax({
		type: "POST",
		url: "comment_process.php",
		data: { comment: $('#commentBox TEXTAREA').val(), name: $('#screenName').val()}
		})
		.done(function( msg ) {
			refreshComments();
			$('#commentBox TEXTAREA').val('');
		});
	}
</script>
<link rel="stylesheet" href="css/comments.css" />
</head>

<body>
    <div class="container">
		<div id='comments'>
		</div>
        <form name='comments' id='commentBox'>
			<div class="textarea-box">
				<textarea rows='10' ></textarea><br />
				<label>Sreen Name
					<input type='text' name='screenName' id='screenName' />
				</label>
				<label>Press Enter To Send
					<input type='checkbox' id='ets' />
				</label>
				<button type='button'>Add Comment</button>
			</div>
        </form>
    </div>
</body>
</html>
