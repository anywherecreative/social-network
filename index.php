<?php
session_start();
require_once('connect.php');
if(isset($_POST['title'])) {
	$title = $mysql->real_escape_string($_POST['title']);
	$article = $mysql->real_escape_string($_POST['article']);
	$mysql->query("INSERT INTO `articles` (`ART_TITLE`,`ART_CONTENT`) VALUES('$title','$article')");
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>My Articles</title>
	<link rel='stylesheet' href='http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css' />
	<link rel='stylesheet' href='css/comments.css' />
	<script src='http://code.jquery.com/jquery-2.1.1.min.js'></script>
	<script src='http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'></script>
	<?php
	if((isset($_GET['I_am_easter']) || $_SESSION['easter']) && !isset($_GET['no_chocolate_for_you'])):
		$_SESSION['easter'] = true;
	?>
		<link rel='stylesheet' href='css/easter.css' />
	<?php 
		endif;
	?>
	<?php
	if((isset($_GET['dance']) || $_SESSION['dance']) && !isset($_GET['party_over'])):
		$_SESSION['dance'] = true;
	?>
		<script>
			var right = false;
			$(document).ready(function() {
				dance();
				setInterval("dance()",469);
			});
			function dance() {
				if(right) {
					$('.container-fluid').addClass('tilt-right')
					$('.container-fluid').removeClass('tilt-left')
					right = false
				}
				else {
					$('.container-fluid').addClass('tilt-left')
					$('.container-fluid').removeClass('tilt-right')
					right = true
				}
				$('DIV H2 SPAN').toggleClass('turn')
			}
		</script>
		<style>
			.tilt-right *:not(.turn) {
				-moz-transform:rotate(1deg);
				-webkit-transform:rotate(1deg);
				-o-transform:rotate(1deg);
				-ms-transform:rotate(1deg);
				transition:all 0.469s;
			}
			.tilt-left *:not(.turn) {
				-moz-transform:rotate(-2deg);
				-webkit-transform:rotate(-2deg);
				-o-transform:rotate(-2deg);
				-ms-transform:rotate(-2deg);
				transition:all 0.469s;
			}
			DIV H2 SPAN {
				transition:all 2;
				display:inline-block;
			}
			DIV H2 SPAN.turn {
				-moz-transform:rotate(360deg);
				-webkit-transform:rotate(360deg);
				-o-transform:rotate(360deg);
				-ms-transform:rotate(360deg);
				transition:all 0.469s;
			}
		</style>
	<?php
	endif;
	if(isset($_GET['no_chocolate_for_you'])) {
		$_SESSION['easter'] = false;
	}
	if(isset($_GET['party_over'])) {
		$_SESSION['dance'] = false;
	}
	?>
	<script>
		$(document).ready(function () {
			$('#newArticle').hide();
			$('NAV BUTTON').click(function() {
				$('#newArticle').show();
			});
			$('#newArticle FORM .cancel').click(function() {
				$('#newArticle').hide();
			});
			$('#commentBox BUTTON').click(function () {
				postComment();
			});
			$('#commentBox TEXTAREA').on('keyup blur',function() {
				if($('#commentBox TEXTAREA').val() == '') {
					$('#commentBox BUTTON').hide();
				}
				else {
					$('#commentBox BUTTON').show();
				}
			});
			$('#commentBox TEXTAREA').keypress(function (e) {
				code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13 && $('#ets').is(":checked")) {
					postComment()
				}
			});
			refreshComments();
			$('#commentBox BUTTON').hide();
			if($('commentBox INPUT[type=hidden]').val() != '') {
				setInterval(refreshComments, 5000);
			}
		});
	function postComment() {
		if($('#commentBox TEXTAREA').val() == '') {
			alert('You can\'t post an empty comment!');
			return false;
		}
		$.ajax({
			type: "POST",
			url: "comment_process.php",
			data: { comment: $('#commentBox TEXTAREA').val(), name: $('#screenName').val(), id: $('#commentBox INPUT[type=hidden]').val()}
			})
			.done(function( msg ) {
				refreshComments();
				$('#commentBox TEXTAREA').val('');
			});
	}
	function refreshComments() {
		if($('#commentBox INPUT[type=hidden]').val() != '') {
			$.ajax({
				type: "GET",
				url: "get_comments.php",
				data: {id: $('#commentBox INPUT[type=hidden]').val()}
				})
				.done(function( msg ) {
					$('#comments').html(msg);
				});
			}
		}
	</script>
	<style type='text/css'>
		ASIDE {
			position:fixed;
			top:0;
			left:0;
			width:100%;
			height:100%;
			background:RGBA(0,0,0,0.8);
		}
		ASIDE > FORM {
			position:fixed;
			top:150px;
			left:25%;
			width:50%;
			min-height:150px;
			background:#fff;
			border-radius:5px;
		}
	</style>
</head>

<body>
	<div class='container-fluid'>
		<div class='row'>
			<div class='col-md-3'>
            	<nav>
                    <h2>My Articles</h2>
                        <?php
                            $result = $mysql->query("SELECT * FROM `articles`");
                            if($result->num_rows > 0):
                                echo('<ul>');
                                while($row = $result->fetch_assoc()): 
                            ?>
                            <li><a href='?id=<?=$row['ART_ID']?>'><?=$row['ART_TITLE']?></a></li>
                        <?php 
                            endwhile;
                                echo('</ul>');
                            else:
                        ?>
                            <h3>There are not articles yet.  Why don't you write one?</h3>
                        <?php
                            endif;
                        ?>
                    <button>New Article</button>
                </nav>
			</div>
			<div class='col-md-9'>
				<?php
				if(isset($_GET['id'])):
					$id = $mysql->real_escape_string($_GET['id']);
					$result = $mysql->query("SELECT * FROM `articles` WHERE `ART_ID` = '$id'");
					if($result->num_rows < 1) {
						?>
							<h1>Oops!</h1>
							<p>Mr. Cactus couldn't find an article with that id.</p>
						<?php
					}
					else {
						$row = $result->fetch_assoc();
						$content = str_ireplace("<script","&lt;script",$row['ART_CONTENT']);
						$content = nl2br($content);
						?>
                        <div class="artice-container">
							<h1><?=$row['ART_TITLE'];?></h1>
							<div>
								<?=$content;?>
							</div>
                        </div>
						<div class="comment_container">
							<div id='comments'>
							</div>
							<form name='comments' id='commentBox'>
								<div class="textarea-box">
									<textarea rows='10' ></textarea><br />
									<label>Sreen Name
										<input type='text' name='screenName' id='screenName' />
									</label>
									<label>Press Enter To Send
										<input type='checkbox' id='ets' />
									</label>
									<input type='hidden' name='id' value="<?=$row['ART_ID']?>" />
									<button type='button'>Add Comment</button>
								</div>
							</form>
						</div>
						<?php
					}
				else :
				?>
					<h1>Welcome</h1>
					<p>Go find an article on the right Homes</p>
				<?php
				endif;
				?>
			</div>
		</div>
	</div>
	<aside id='newArticle'>
		<form action='index.php' method='post'>
			<h1>New Article</h1>
			<label>Title
				<input type='text' name='title' placeholder='title' />
			</label>
			<label>Article
				<textarea placeholder='Article' name='article'></textarea>
			</label>
			<button type='button' class='cancel'>Cancel</button>
			<button type='submit'>Add</button> 
		</form>
	</aside>
</body>
</html>
